#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import shutil
import cherrypy
import json
import config as cfg
import main
import datetime
import sqlite3


config = {
    'global': {
        'server.socket_host': '127.0.0.1',
        'server.socket_port': 8080,
        'server.thread_pool': 8,
        'response.timeout': 7200,        
        'cors.expose.on': True
    },
    '/img': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': os.path.join(os.getcwd().replace("\\", "/"), "img"),
    }

}

def createConnection():
    # Database File for SQLite
    # db_file = "core_database/tufin_reqs.db # Uncomment if running on Linux
    db_file = "C:\\Users\\chaudhary\\Documents\\tufin_py\\jang1\\core_database\\tufin_reqs.db"
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

class App:
  @cherrypy.expose
  def index(self):
    return open('index.html')

  @cherrypy.expose
  def upload(self):
    '''Handle non-multipart upload'''

    filename = os.path.basename(cherrypy.request.headers['x-filename'])
    destination = os.path.join("C:\\Users\\chaudhary\\Documents\\tufin_py\\jang1", "excel_parse.xlsx")
    with open(destination, 'wb') as f:
      shutil.copyfileobj(cherrypy.request.body, f)
    
    return json.dumps({'success':True})   

  @cherrypy.expose()
  @cherrypy.tools.json_out()
  @cherrypy.tools.json_in()
  def run_job(self):
    result = {"operation": "request", "result": "success"}
    input_json = cherrypy.request.json
    jid = input_json['job_id']
    main.main(jid)

  @cherrypy.expose()
  @cherrypy.tools.json_out()
  @cherrypy.tools.json_in()
  def dbSelectJobDetailsHelper(self):
      result = {"operation": "request", "result": "success"}
      input_json = cherrypy.request.json
      id = input_json['id']
      sql = "select msg from main_table where id='"+id+"' order by pk"   
      conn = createConnection();   
      cur = conn.cursor()
      rows = list(cur.execute(sql))               
      conn.close()
      return rows

  @cherrypy.expose()
  def auth(self, userName, userPassword):
    sql = "select count(*) from auth_table where username='"+userName+"' and password = '" + userPassword + "'"
    conn = createConnection();   
    cur = conn.cursor()
    rows = list(cur.execute(sql))
    conn.close()
    authorized = False
    if rows[0][0] == 1:
      authorized = True    
    if authorized == True:
        return open('home.html')
    else:
        return open('index_failed.html')

if __name__ == '__main__':
  cherrypy.quickstart(App(), '/', config)