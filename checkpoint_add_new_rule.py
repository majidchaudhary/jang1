######################################################
#
#
# Module to represent working methods with access rules
#               throught API
#
#
# Python 3.6
#####################################################

from pprint import pprint
#For computing time-perfomance
from datetime import datetime
import math
from configparser import ConfigParser
import sys, os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# cpapi is a library that handles the communication with the Check Point management server.
from cpapi import APIClient, APIClientArgs

#For testing purposes - define some constant values via config.ini
config = ConfigParser()
config.read('config.ini')
USER = config['DEFAULT']['USER']
PASS = config['DEFAULT']['PASS']


def _full_hosts_obj_base(api_client, chk_obj, layer='Network'):
    '''
    This function return dictionary with full hosts objects for future calculation.
    Also it can be used only within API_Client - cause required this object as input parameter.

    :param api_client:[obj] - OBJ of api-client to which we connect
    :param layer:[str] - layer to research of rules count
    :return: Dictionary with hosts objects
    '''
    # Dictionary with init  parameters for API-call,
    #by default limit == 300 -- can tune this parameter for perfomance counters
    if chk_obj[0].isdigit():
        _dict = {
            "limit": 500,
            "offset": 0,
            "details-level": "standard",
             "type" : "object",
            "filter" : 'new_host_tufin_'+chk_obj[0:4]            
        }
    else:
        _dict = {
            "limit": 500,
            "offset": 0,
            "details-level": "standard",
            "order": [{
                "ASC": "name"
            }],
            "type" : "access-role",
            "filter" : "Sec-FW-"
        }

    result = api_client.api_call('show-objects', payload=_dict)
    if result.success is False:
        print(result.error_message)
        exit(-1)  # means that we not able to connect with server or recieve API-response - so we exit..
    # Recieve JSON  notation - put that data into dictionary
    j_dict = result.data

    # Find count of iterations for research all base of hosts
    if j_dict['total'] > j_dict['to']:
        iterations = math.ceil(j_dict['total'] / _dict['limit'])
        # Start cycle to filling up full _j_dict with data
        _dict['offset'] = j_dict['to']  # Init start offset for second iteration
        for _iter in range(iterations - 1):
            _result = api_client.api_call('show-hosts', payload=_dict)
            if _result.success is False:
                print(_result.error_message)
                exit(-1)  # means that we not able to connect with server or recieve API-response - so we exit..
            _j_dict = _result.data
            _dict['offset'] = _j_dict['to']  # Get new offset value
            j_dict['objects'].extend(_j_dict['objects'])  # Append dictionaries to the end of storage_object

    # Else - we don't need to iterate any more - can proceed to matching (ip,uid)

    return j_dict


## WARNING! Actually UID of ANY object can be hardcoded as '97aeb369-9aea-11d5-bd16-0090272ccb30' in config.ini
# But I can't find any provement of theory that this UID is same on all CheckPoint Boxes :(
#We can find it through API-call (set 'any' /delete 'any') and put in config.ini (but for PoC won't do it -> to backlog)
def total_count(api_client,layer='Network'):
    '''
    This function return the total count of rules that stored in SMS under 'Network' layer
    before 'last' CLEANUP rule. WARNING!! Used name of rule to find cleanup rule!
    Other words - position of CLEANUP rule and additionally UID of ANY obj(internal logic)
    :param api_client:[obj] - OBJ of api-client to which we connect
    :param layer:[str] - layer to research of rules count
    :return: Tuple of  position of CLEANUP rule and UID of 'Any' object
    '''
    _dict = {
                "offset": 0,
                "limit": 5,
                "name": layer,
                "details-level": "uid",
                "use-object-dictionary": False,
            }
    result = api_client.api_call('show-access-rulebase', payload=_dict)
    if result.success is False:
        print(result.error_message)
        exit(-1) #means that we not able to connect with server or recieve API-response - so we exit..
    _j_dict = result.data
    #This is last rule
    number = _j_dict['total']
    #HARDCODE! any uid
    any_uid = '97aeb369-9aea-11d5-bd16-0090272ccb30'
    return number,any_uid



def add_new_rule(sms, chpt_gw=None, access_role_name=None, source=None, destination=None,
                 service=None, Comment=None, action='Accept', type_of_vars='name', verify=False, **payload):
    '''
    Add new rule to policy and publish (without install)
    :param sms:[str] IP of SMS CheckPoint
    :param chpt_gw:[str] - UID of object, if None - then targets is all
    :param access_role_name:[str or list of str] - name of access role(s) or UID
    :param source:[str or list of str] - source IP-address(es) or subnet(s) or UID
    :param destination:[str or list of str] - destination IP-address(es) or subnet(s) or UID
    :param service:[str or list of str] - type of service(s) (define by _name_ or UID)
    :param Comment:[str] - comment to fill out Custom string
    :param action:[str] - action withing rule ( default - 'Accept')
    :param type_of_vars:['name'|'uid'] - define which type of vars we waiting in access_role_name and others parameters (src,dst,service). Default - 'name'
    :param verify:[Boolean] - if True verify is that rule will be unique in current sms&chpt_gw (all input args will be checked).
     If not unique - return UID & position of dublicated rule(s). Work only if we recieve UID of objects (type_of_vars='uid')
    :param payload:[dict] - dict notation of JSON,that represent additional parameters which can be used in 'add-access-rule' API in the future
    :return:[Boolean] True - if success, false otherwise
    '''

    #Fill mandatory parameters in API-call for add_rule - these one is hardcoded (as default), but we can redefine these parameters in **params,
    #that we transfer to the function (we update api_dict, before post this)
    api_payload =  {
        "layer": "Network"}
    # 'position' key is also mandatory - will fill them later
    # SHOULD BE UID of CLEANUP RULE or number, cause we cannot rely on Name of Rule!!


    # Define all needed arguments as lists to avoid mismatching types while do operations
    # If some parameter is None (by default) - this is means ANY object in  access rule
    if isinstance(access_role_name,str):
        access_role_name =[access_role_name]
    if isinstance(source,str):
        source=[source]
    if isinstance(destination,str):
        destination=[destination]
    if isinstance(service,str):
        service=[service]

    # Create new list of source - if there are access role & source at the same time
    if all([source,access_role_name]):
        source.extend(access_role_name)
    elif access_role_name:
        source = access_role_name


    # Verify type_of_vars parameter
    if any([type_of_vars=='name',type_of_vars=='uid']) is False:
        print(f'For type_of_vars was defined bad value - {type_of_vars} . \n'
              f' For avoid unexpected changes on production - function will not executed\n')
        raise ValueError(f"Unexpected type. Value can't be {type_of_vars}\n")


    #We can't verify other args, cause we work with them as a objects.
    # So, this can be done only if we have DB of all objects - and query to it,
    # or we can refer to SMS dynamicly  in time of exec our module - but this is
    # time-consuming and expensive for computation of SMS  (for PoC we omitting this part)


    # Work with parametrs that have been transferred throught **params
    # Will do one common dictionary - api_payload

    api_payload['source'] = source
    api_payload['destination'] = destination
    api_payload['service'] = service
    api_payload['action'] = action
    api_payload['install-on'] = chpt_gw
    api_payload['custom-fields'] = {"field-1": Comment}
    if payload:
        api_payload.update(payload)



    #Define parameters for initial connection and create connection
    with APIClient(APIClientArgs(server=sms,unsafe=True)) as conn:
        # The API client, would look for the server's certificate SHA1 fingerprint in a file.
        # If the fingerprint is not found on the file, it will ask the user if he accepts the server's fingerprint.
        # In case the user does not accept the fingerprint, exit the program.
        # IF WE DON'T WANT  TO CHECK FINGERPRINT -> set unsafe mode = True
        if conn.check_fingerprint() is False:
            print("Could not get the server's fingerprint - Check connectivity with the server.\n")
            exit(1)
        #init login
        login_res = conn.login(username='GPB_tufin_PoC_API',password='1q2w3e')#PASS

        # Verify (is all right?)
        if login_res.success is False:
            print(f"Login failed:\n{login_res.error_message}"
                  f"\n Please verify login credentinals in config.ini file\n")
            exit(1)

        # Here we get all  obj-base of hosts
        full_base_dict = _full_hosts_obj_base(conn, source[0])

        # Find UID of object from defined IP-address of SOURCE!
        # And match in list of UIDs (SOURCES)
        list_of_match_sources = []
        for item in full_base_dict['objects']:            
            for _ip in source:
                if _ip[0].isdigit():
                    if item['name'][0:14] == 'new_host_tufin':
                        if item['ipv4-address'] == _ip:
                            list_of_match_sources.append(item['name'])
                else:                    
                    if item['name'] == _ip:
                        list_of_match_sources.append(item['name'])

        full_base_dict = _full_hosts_obj_base(conn, destination[0])
        # Find UID of object from defined IP-address of DESTINATION!
        # And match in list of UIDs (destination)
        list_of_match_dest = []
        for item in full_base_dict['objects']:
            if item['name'][0:14] == 'new_host_tufin':
                for _ip in destination:                                       
                    if item['ipv4-address'] == _ip:
                        list_of_match_dest.append(item['name'])


        ######## REDIFINE API PAYLOAD ################# NEED REFACTOR!!!
        ############# FORK !!!
        if any([list_of_match_sources,list_of_match_dest]):
            #print('WE FIND THAT AT LEAST ONE FIELD IS FILLED')
            if all([len(source)==len(list_of_match_sources),len(destination)==len(list_of_match_dest)]):
                api_payload['source'] = list_of_match_sources
                api_payload['destination'] = list_of_match_dest
            else:
                print('SOME OBJECT WAS NOT FIND IN OBJECT DICTIONARY - NEED TO CREATE THEM'
                      ' AND TRY EXECUTE SCRIPT AFTER THIS!\n'
                      'DUE AVOID UNPREDICTABLE BEHAVIOUR - SCRIPT REJECT ADDING THIS RULE\n\n')
                return False
        else:
            print('SOME OBJECT WAS NOT FIND IN OBJECT DICTIONARY - NEED TO CREATE THEM'
                  ' AND TRY EXECUTE SCRIPT AFTER THIS!\n'
                  'DUE AVOID UNPREDICTABLE BEHAVIOUR - SCRIPT REJECT ADDING THIS RULE\n\n')
            return False
        ###############################



        #Find total number of rules before CLEANUP
        _position,any_uid = total_count(conn)
        api_payload['position']=_position

        #Fork to work with VERIFY flag
        #in case  of matching - will return tuple of lists of position_nuber and UID of dublicate rule
        if all([verify,type_of_vars=='uid']):
            # verify - is there the same rule in list of rules?
            # We should expore all rules in current DB of SMS within all layers
            _temp_payload = {
                "offset": 0,
                "limit": 200,
                "name": "Network",
                "details-level": "uid"
            }
            ref = dict()
            #Need to sort existing lists - otherwise matching willn't perform correctly
            if source:
                source.sort()
            else:
                source = any_uid
            if destination:
                destination.sort()
            else:
                destination = any_uid
            if service:
                service.sort()
            else:
                service = any_uid
            iteration = math.ceil(_position / 200)
            match_position = []
            uid = []
            for iter in range(iteration):
                _temp_res = conn.api_call('show-access-rulebase', payload=_temp_payload)
                for item in _temp_res.data['rulebase']:
                    if item['source']:
                        item['source'].sort()
                    if item['destination']:
                        item['destination'].sort()
                    if item['service']:
                        item['service'].sort()
                    if all([item['source'] == source,
                            item['destination']== destination,
                            item['service']== service]):
                        match_position.append(item['rule-number'])
                        uid.append(item['uid'])
                    #else: print(f'{item["rule-number"]} not match')
            if match_position:
                return match_position,uid

        result = conn.api_call('add-access-rule',payload=api_payload)

        if result.success:
            # publish the result
            publish_res = conn.api_call("publish", {})
            if publish_res.success:
                print("The changes were published successfully.")
            else:
                print("Failed to publish the changes.")
        else:
            print(result.error_message)
            return False
    # Logout from SMS automaticaly when destroyed APIClient object

    return True


if __name__=='__main__':
    start_time = datetime.now()
    params={"comment":"TEST 10.06.2020"}

    #add_new_rule(sms, chpt_gw=None, access_role_name=None, source=None, destination=None,
    #             service=None, comment=None, action='Accept', type_of_vars='name', verify=False, **payload)

    #pprint(add_new_rule('10.1.101.20',source=['New Access Role 1'],destination='New Host 1',action='Drop',**params))


    print(datetime.now() - start_time)
