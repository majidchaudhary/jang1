param(
    [string] $NsxManagerIP,
    [string] $Action,
    [string] $RuleId,
    [string] $RegExp,
    [string] $JsonQueryAsString
)

#region DebugParams
if($NsxManagerIP -eq ""){
    Write-Host "Debug mode"
    [string] $NsxManagerIP = "10.1.101.72"
    [string] $Action = "Create"
    [string] $RuleId="1072"    #$RuleId ="1019"
    [string] $RegExp = "nsx-test2"
    [string] $JsonQueryAsString = "['Другие администраторы', [], [['10.96.96.96', 'none'], ['10.96.96.97', 'none'], ['10.96.96.98', 'none']],  [['TCP','22,80,8080'], ['UDP', '53, 514']], 'Тестовый комментарий второй']"
    [bool] $InteractiveMode = $false
}else{
    [bool] $InteractiveMode = $false # Если запускаем скриптом с IP NSX-а подразумевается автоматический режим выполнения, сбросим переменную на всякий
}
#endregion DebugParams

#region ModuleVariables
$NsxManagerUser = "admin "
$NsxManagerPassword="P@ssw0rd"
$QueryJson = $JsonQueryAsString | ConvertFrom-Json
$finish = $true
#endregion ModuleVariables



#$SectionGroupID = "1003"
#region Enums
Enum ModuleAction{
    Create
    Update
    Delete
    Show
}

Enum EndpointObjectType{
    SecurityGroup;
    IpSet;
    Cluster;
    Datacenter;
    DistributedPortGroup;
    LegacyPortGroup;
    LogicalSwitch;
    ResourcePool;
    VM;
    VApp;
    vNIC
}

Enum SecurityGroupMemberTypeType{
    IpSet;
    MacSet;
    SecurityTag;
    Cluster;
    Datacenter;
    DirectoryGroup;
    DistributedPortGroup;
    LegacyPortGroup;
    LogicalSwitch;
    ResourcePool;
    VM;
    VApp;
    vNIC
}
Enum RawProtocolType{
    TCP;
    UDP;
    NBNS_BROADCAST;
    NBDG_BROADCAST;
    ICMP;
    IGMP;
    IPCOMP;
    RSVP;
    GRE;
    ESP;
    AH;
    L2TP;
    SCTP;
}
 
Enum SecurityGroupDynamicMemberFilterField{
    Entity;
    SecurityTag;
    VMName;
    ComputerOSName;
    ComputerName;
}

Enum SecurityGroupDynamicMemberFilterType{
    Contains;
    EndWith;
    EqualsTo;
    BelongsTo;
    NotEqualsTo;
    StartWith;
    MatchRegex;
}

#endregion Enums

class Ipv4Addr{
    [int] $Oct1
    [int] $Oct2
    [int] $Oct3
    [int] $Oct4
    #Constructor as Octets
    Ipv4Addr([byte] $Oct1,[byte] $Oct2,[byte] $Oct3,[byte] $Oct4){
        #/TODO/ Add Check range
        $this.Oct1 = $Oct1
        $this.Oct2 = $Oct2
        $this.Oct3 = $Oct3
        $this.Oct4 = $Oct4
    }
    Ipv4Addr([string] $StrIp){
        $IPValue = $StrIp.Split(".")
        $this.Oct1 = [byte]$IPValue[0]
        $this.Oct2 = [byte]$IPValue[1]
        $this.Oct3 = [byte]$IPValue[2]
        $this.Oct4 = [byte]$IPValue[3]
    }
    [bool] IsSame([Ipv4Addr] $Addr){
        if(($this.Oct1 -eq $Addr.Oct1) -and ($this.Oct2 -eq $Addr.Oct2) -and ($this.Oct3 -eq $Addr.Oct3) -and ($this.Oct4 -eq $Addr.Oct4)) { 
            return $true
        } else {
            return $false
        }
    }
    [string] GetAsString() {
        return [string](""+$this.Oct1+"."+$this.Oct2+"."+$this.Oct3+"."+$this.Oct4)
    }
}

#Source or Destination object in firewall rule
class EndpointObject{
    [EndpointObjectType] $Type # SecurityGroup IpSet Cluster Datacenter DistributedPortGroup LegacyPortGroup LogicalSwitch ResourcePool VM VApp vNIC
    [string] $Name
    [string] $Value
    EndpointObject([EndpointObjectType] $Type, [string] $Name, [string] $Value){
        $this.Type = $Type
        $this.Name = $Name
        $this.Value = $Value
    }
    [bool] IsContainer(){
        [bool] $Result = $false
        if(($this.Type -eq [EndpointObjectType]::SecurityGroup) -or
            ($this.Type -eq [EndpointObjectType]::DistributedPortGroup) -or
            ($this.Type -eq [EndpointObjectType]::LegacyPortGroup)){
                $Result = $true
            }
        return $Result
    }
    [bool] MatchFilterCriteria([RestController] $Obj,[EndpointObject] $FilterObject){
        $Matched = $false
        if($this.Type -eq $FilterObject.Type){
            if($this.Name -eq $FilterObject.Name){
                $Matched = $true
            }
        }elseif($this.IsContainer() -and (-not $FilterObject.IsContainer())){
            #/TODO/ Search throught groups`
            $Matched = $false
            if($FilterObject.Type -eq "VM"){
                #$obj = New-Object RestController($NsxManagerUser,$NsxManagerPassword,"https://$NsxManagerIP")
                if($this.Type -eq "SecurityGroup"){
                    #$SecGroups = $obj.QuerySecurityGroups()
                    $VMs = $obj.QueryVMBySecGroupID($this.Value)
                    foreach ($VM in $VMs) {
                        if($VM -eq $FilterObject.Name){
                            $Matched = $true
                            Break
                        }
                    }
                    <#
                    $SecGroups = $obj.QuerySecurityGroupsByVMId($FilterObject.Value)
                    foreach($SecGroup in $SecGroups){
                        if($SecGroup.Name -eq $this.Name){
                            $Matched = $true
                            Break
                        }
                    }
                    #>
                }#/TODO/ search in port groups
            }
        }
        return $Matched
    }
}

class Endpoint{
    [bool] $IsAny
    [Ipv4Addr[]] $IpAddesses
    [EndpointObject[]] $Objects
    [bool] ContainIp([Ipv4Addr]$Ipaddr){
        foreach($IP in $this.IpAddesses){
            if($IP.IsSame($Ipaddr)) {
                return $true
            }
        }
        return $false
    }
    [bool] MatchFilterCriteria([RestController] $obj,[EndpointObject] $FilterObject){
        $Matched = $False
        foreach($EndpointObject in $this.Objects){
            if($EndpointObject.MatchFilterCriteria($obj,$FilterObject)){
                $Matched = $true
                Break
            }
        }
        return $Matched
    }
    [xml] GetAsXML(){
        $Result=""
        $Result += "<sources excluded=`"false`">"
        if($null -ne $this.IpAddesses){
            $this.IpAddesses | ForEach-Object {
                $IPAddress = $_.GetAsString()
                $Result += "<source>
                <value>$IPAddress</value>
                <type>Ipv4Address</type>
                <isValid>true</isValid>
                </source>"
            }
        }
        if($null -ne $this.Objects){
            $this.Objects | ForEach-Object {
                #$_
                $Result += "<source>
                <value>$($_.Name)</value>
                <type>SecurityGroup</type>
                <isValid>true</isValid>
                </source>"                
            }
        }        
        $Result+="</sources>"
    
        return [xml] $Result    
    }
    [xml] GetAsXMLDest(){
        $Result=""
        $Result += "<destinations excluded=`"false`">"
        if($null -ne $this.IpAddesses){
            $this.IpAddesses | ForEach-Object {
                $IPAddress = $_.GetAsString()
                $Result += "<destination>
                <value>$IPAddress</value>
                <type>Ipv4Address</type>
                <isValid>true</isValid>
                </destination>"
            }
        }
        $Result+="</destinations>"
        return [xml] $Result    
    }
}


class RawProtocolEntry{
    [RawProtocolType] $Type
    [int] $SourcePort # 0..65536  -1 Any
    [int] $DestPort
    RawProtocolEntry([RawProtocolType] $Type, [int] $SourcePort, [int] $DestPort){
        $this.Type = $Type
        $this.SourcePort = $SourcePort
        $this.DestPort = $DestPort
    }
    [string] GetAsString(){
        $Result = ""
        if($this.Type -eq [RawProtocolType]::TCP){
            #$Result +="TCP-$($this.SourcePort)=>$($this.DestPort)"
            if(0 -ne $this.SourcePort){
                $Result +="TCP $($this.SourcePort)"
            }elseif(0 -ne $this.DestPort){
                $Result +="TCP $($this.DestPort)"
            }else{
                $Result +="TCP Any"
            }
        }elseif($this.Type -eq [RawProtocolType]::UDP){
            #$Result +="UDP-$($this.SourcePort)=>$($this.DestPort)"
            if(0 -ne $this.SourcePort){
                $Result +="UDP $($this.SourcePort)"
            }elseif(0 -ne $this.DestPort){
                $Result +="UDP $($this.DestPort)"
            }else{
                $Result +="UDP Any"
            }
        }else{
            $Result = "$($this.Type)"
        }
        return $Result
    }
    #SourceDest source = 0 dest=1 both=2 (check TCP UDP based on source or dest port) default both
    [bool] MatchPort([RawProtocolType] $RawProtocolType,[int] $Port=$null,[int] $SourceDest=2){
        $Matched = $false
        if($this.Type -eq $RawProtocolType){
            if(($RawProtocolType -eq [RawProtocolType]::TCP) -or ($RawProtocolType -eq [RawProtocolType]::UDP) ){
                #Needed to check Ports
                if($Port -ne 0){
                    if(($SourceDest -eq 0) -or ($SourceDest -eq 2)){
                        #check source port equality
                        if(($this.SourcePort -eq -1) -or ($this.SourcePort -eq $Port)){
                            $Matched = $true
                        }
                    }
                    if(-not $Matched){
                        if(($SourceDest -eq 0) -or ($SourceDest -eq 1)){
                            if(($this.DestPort -eq -1) -or ($this.DestPort -eq $Port)){
                                $Matched = $true
                            }
                        }
                    }
                }
            }else{
                #For other protocols check only type
                $Matched = $true
            }
        }
        return $Matched
    }
}

class ServiceEntry{
    [string] $Name
    [string] $Value
    [string] $Type
    ServiceEntry($Name,$Value="",$Type=""){
        $this.Name = $Name
        $this.Value = $Value
        $this.Type = $Type
    }
}

class RuleService {
    [RawProtocolEntry[]] $RawProtocolEntrys
    [ServiceEntry[]] $ServiceEntrys
    #Return true if also in one entry the same source or destination port and protocol type
    [bool] MatchFilterCriteria([RawProtocolEntry] $FilterRawProtocolEntry){
        $Matched = $false
        foreach($RawProtocolEntry in $this.RawProtocolEntrys){
            if($RawProtocolEntry.MatchPort($FilterRawProtocolEntry.Type,$FilterRawProtocolEntry.SourcePort,0)) {
                $Matched = $true
                Break
            }
            if($RawProtocolEntry.MatchPort($FilterRawProtocolEntry.Type,$FilterRawProtocolEntry.DestPort,1)) {
                $Matched = $true
                Break
            }
        }
        return $Matched
    }
    [bool] MatchFilterCriteria([ServiceEntry] $FilterServiceEntry){
        $Matched = $false
        foreach($ServiceEntry in $this.ServiceEntrys){
            if($FilterServiceEntry.Name -eq $ServiceEntry.Name) {$Matched = $true}
            if($Matched -eq $true) {Break}
        }
        return $Matched
    }
    [bool] MatchFilterCriteria([RuleService] $RuleService){
        $Matched = $false
        foreach($FilterRawProtocolEntry in $RuleService.RawProtocolEntrys){
            if($this.MatchFilterCriteria($FilterRawProtocolEntry)){
                $Matched = $true;
                Break;
            }

        }
        #Search by services
        if(-not $Matched){
            foreach($FilterServiceEntry in $RuleService.ServiceEntrys){
                if($this.MatchFilterCriteria($FilterServiceEntry)){
                    $Matched = $true
                    Break
                }
            }
        }

        return $Matched
    }

    [xml] GetAsXML(){
        $Result = "<services>"
        $this.RawProtocolEntrys | ForEach-Object {
            #$_.Type
            #$_.SourcePort
            $SerializedType = ""
            $ProtocolNumber = 0
            $SubProtocolNumber=6
            $SubProtocolName = ""
            $UsePorts = $false
            $UseSubProtocol = $false
            if($_.Type -eq [RawProtocolType]::TCP) {
                $SerializedType="TCP"
                $ProtocolNumber = 6
                $UsePorts = $true
                $UseSubProtocol = $true
            }elseif ($_.Type -eq [RawProtocolType]::UDP) {
                $SerializedType="UDP"
                $ProtocolNumber = 17    
                $UsePorts = $true
                $UseSubProtocol = $true
            }elseif ($_.Type -eq [RawProtocolType]::ICMP) {
                $SerializedType="ICMP"  
                $ProtocolNumber = 1 
                $UseSubProtocol = $true
                $SubProtocolNumber = 1 
            }elseif ($_.Type -eq [RawProtocolType]::IGMP) {
                $SerializedType="IGMP"  
                $ProtocolNumber = 2  
            }elseif ($_.Type -eq [RawProtocolType]::ESP) {
                $SerializedType="ESP"  
                $ProtocolNumber = 50  
            }elseif ($_.Type -eq [RawProtocolType]::L2TP) {
                $SerializedType="L2TP"  
                $ProtocolNumber = 115  
            }elseif ($_.Type -eq [RawProtocolType]::SCTP) {
                $SerializedType="SCTP"  
                $ProtocolNumber = 132  
            }elseif ($_.Type -eq [RawProtocolType]::AH) {
                $SerializedType="AH"  
                $ProtocolNumber = 51  
            }elseif ($_.Type -eq [RawProtocolType]::IPCOMP) {
                $SerializedType="IPCOMP"  
                $ProtocolNumber = 108  
            }elseif ($_.Type -eq [RawProtocolType]::RSVP) {
                $SerializedType="RSVP"  
                $ProtocolNumber = 46  
            }elseif ($_.Type -eq [RawProtocolType]::GRE) {
                $SerializedType="GRE"  
                $ProtocolNumber = 47  
            }
            $PortParts = ""
            $SubProtocolPart =""
            if($UsePorts){
                $PortParts = "<sourcePort>$($_.sourcePort)</sourcePort>
                <destinationPort>$($_.DestPort)</destinationPort>" 
            }
            if($UseSubProtocol){
                $SubProtocolPart = "<subProtocol>$SubProtocolNumber</subProtocol>"
            }
            $Result += ("<service>
            <isValid>true</isValid>
            $PortParts
            <protocol>$ProtocolNumber</protocol>
            $SubProtocolPart
            <protocolName>$SerializedType</protocolName>
            </service>" )       
        }
        $this.ServiceEntrys | ForEach-Object {
            if($_ -ne $null){
            $Result += "<service>
            <isValid>true</isValid>
            <name>$($_.name)</name>
            <value>$($_.value)</value>
            <type>$($_.type)</type>
            </service>"
            }
        }
        $Result += "</services>"
        return [xml] $Result
    }
}

class RuleDescription{
    [int] $ID = 0
    [string] $Name
    [Endpoint] $Source
    [Endpoint] $Destination
    [RuleService] $Service
    RuleDescription([string] $Name,[Endpoint] $Source,[Endpoint] $Destination,[RuleService]$Service){
        $this.Name = $Name
        $this.Source = $Source
        $this.Destination = $Destination
        $this.Service = $Service
    }
    [xml] GetAsXML(){
        [xml] $xmlBody = (@"
<rule disabled=`"false`" logged=`"true`">
<name>$($this.Name)</name>
<action>allow</action>
<appliedToList>
<appliedTo>
<name>DISTRIBUTED_FIREWALL</name>
<value>DISTRIBUTED_FIREWALL</value>
<type>DISTRIBUTED_FIREWALL</type>
<isValid>true</isValid>
</appliedTo>
</appliedToList>
$($this.Source.GetAsXML().InnerXml)
$($this.Destination.GetAsXMLDest().InnerXml)
$($this.Service.GetAsXML().InnerXml)
<direction>inout</direction>
<packetType>any</packetType>
</rule>
"@)
    return $xmlBody
    }
}

class SectionDescription{
    [int] $id
    [int64] $Etag
}

class SecurityGroupDynamicMemberEntry{
    [SecurityGroupDynamicMemberFilterField] $Field #Entity SecurityTag VMName ComputerOSName ComputerName
    [SecurityGroupDynamicMemberFilterType] $Type #Contains EndWith EqualsTo NotEqualsTo StartWith MatchRegex
    [object] $Value
    SecurityGroupDynamicMemberEntry([SecurityGroupDynamicMemberFilterField] $Field,[SecurityGroupDynamicMemberFilterType] $Type,[object] $Value){
        $this.Field = $Field
        $this.Type = $Type
        $this.Value = $Value
    }
}

class SecurityGroupStaticMemberEntry{
    [SecurityGroupMemberTypeType] $Type #IpSet MacSet SecurityTag Cluster Datacenter DirectoryGroup DistributedPortGroup LegacyPortGroup LogicalSwitch ResourcePool VM VApp vNIC
    [object] $Value

}

class SecurityGroup{
    [string] $ObjectID = ""
    [string] $Name
    [string] $Description
    [bool] $IsORJoinDynamicMembers# OR any
    [SecurityGroupDynamicMemberEntry[]] $DynamicMemberEntrys
    [SecurityGroupStaticMemberEntry[]] $Include
    [SecurityGroupStaticMemberEntry[]] $Exclude

    [xml] GetDynamicMemberAsXML(){
        $xmlBody = "<dynamicSet>
        <operator>OR</operator>
<dynamicCriteria>
       <operator>AND</operator>
       <key>VM.NAME</key>
       <criteria>contains</criteria>
       <value>test</value>
     </dynamicCriteria>
     <dynamicCriteria>
       <operator>AND</operator>
       <key>ENTITY</key>
       <criteria>belongs_to</criteria>
       <value>network-243</value>
     </dynamicCriteria>
        </dynamicSet>"
        $xmlBody = "<dynamicSetDtoList><operator>OR</operator>"
        foreach($DME in $this.DynamicMemberEntrys){
            $xmlPart = "<dynamicCriteria>"
            $xmlPart += "<operator>AND</operator>"
            if($DME.Field -eq [SecurityGroupDynamicMemberFilterField]::Entity){
                $xmlPart += "<key>ENTITY</key>"
            }elseif($DME.Field -eq [SecurityGroupDynamicMemberFilterField]::VMName){
                $xmlPart += "<key>VM.NAME</key>"
            }
            if($DME.Type -eq [SecurityGroupDynamicMemberFilterType]::BelongsTo){
                $xmlPart += "<criteria>belongs_to</criteria>"
            }elseif($DME.Type -eq [SecurityGroupDynamicMemberFilterType]::Contains){
                $xmlPart += "<criteria>contains</criteria>"
            }elseif($DME.Type -eq [SecurityGroupDynamicMemberFilterType]::MatchRegex){
                $xmlPart += "<criteria>similar_to</criteria>"
            }
            $xmlPart += "<value>$($DME.Value)</value>"
            $xmlPart += "<isValid>True</isValid>"
            $xmlPart += "</dynamicCriteria>"
            $xmlBody += $xmlPart
        }
        $xmlBody += "</dynamicSetDtoList>"

<#     
#>        
return [xml] $xmlBody
    }
    [xml] GetStaticMembersAsXML(){
        $xmlBody = ""
        $this.Include | ForEach-Object {
            if($_.Type -eq [SecurityGroupMemberTypeType]::VM){
                $VmID = $_.Value
            $xmlBody +="
            <member>
               <objectId>$VmID</objectId>
               <objectTypeName>VirtualMachine</objectTypeName>
                <vsmUuid>42185106-7DEE-B95E-3E0B-714F247F0CC2</vsmUuid>
                <revision>1</revision>
               <type>
                 <typeName>VirtualMachine</typeName>
               </type>
            <scope>
               <id>datacenter-21</id>
                <objectTypeName>Datacenter</objectTypeName>
                <name>DC-01</name>
             </scope>
             </member>"
            }
        }
        #<nodeId>1f5913d4-4e1d-4fea-9641-9b49d61df263</nodeId>
        return [xml] $xmlBody
    }

    [xml] GetAsXML(){
 <#       [xml] $xmlBody = (@"
        <securitygroup>
         <name>$($this.Name)</name>
         <description>$($this.description)</description>
         <objectTypeName>SecurityGroup</objectTypeName>
         <vsmUuid>42185106-7DEE-B95E-3E0B-714F247F0CC2</vsmUuid>
         <revision>1</revision>
            <type>
              <typeName>SecurityGroup</typeName>
            </type>
            <extendedAttributes>
            <extendedAttribute>
                <name>localMembersOnly</name>
                    <value>false</value>
                </extendedAttribute>
            </extendedAttributes>
        $($this.GetStaticMembersAsXML().InnerXml)
        <excludeMember></excludeMember>
        <dynamicMemberDefinition>
        $($this.GetDynamicMemberAsXML().InnerXml)
           </dynamicMemberDefinition>
        </securitygroup>
"@)
#>

[xml] $xmlBody = (@"
<securitygroup>
    <objectTypeName>SecurityGroup</objectTypeName>
    <vsmUuid>42185106-7DEE-B95E-3E0B-714F247F0CC2</vsmUuid>
    <nodeId>1f5913d4-4e1d-4fea-9641-9b49d61df263</nodeId>
    <type>
        <typeName>SecurityGroup</typeName>
    </type>
    <name>$($this.Name)</name>
    <description>$($this.Name)</description>
    <scope>
        <id>globalroot-0</id>
        <objectTypeName>GlobalRoot</objectTypeName>
        <name>Global</name>
    </scope>
    <clientHandle></clientHandle>
    <extendedAttributes>
        <extendedAttribute>
            <name>localMembersOnly</name>
            <value>false</value>
        </extendedAttribute>
    </extendedAttributes>
    <isUniversal>false</isUniversal>
    <universalRevision>0</universalRevision>
    <isTemporal>false</isTemporal>
    <inheritanceAllowed>false</inheritanceAllowed>
    <dynamicMemberDefinition>$($this.GetDynamicMemberAsXML().InnerXml)</dynamicMemberDefinition>
</securitygroup>
"@)
#https://10.1.101.72/api/2.0/services/securitygroup/bulk/globalroot-0
    return $xmlBody
    }
}

class RestController{
    $headers=""
    $cred=""
    $UriBase = ""
    $Result = ""
    $ResultHeaders = ""
    RestController($UserName,$PlainPasswd,$UriBase)
    {
        $password_base64 = ConvertTo-SecureString $PlainPasswd -AsPlainText -Force  
        $this.cred = New-Object System.Management.Automation.PSCredential ($UserName, $password_base64) 
        $EncodedAuthorization = [System.Text.Encoding]::UTF8.GetBytes($UserName + ':' + $PlainPasswd)
        $Auth = [System.Convert]::ToBase64String($EncodedAuthorization)
        $this.headers = @{"Authorization" = "Basic $Auth"}
        $this.headers.Add("Accept", "application/json")
        $this.UriBase = $UriBase
    }

    CallRestAPI($RelURL)
    {
        $uri = ""+$this.UriBase+$RelURL
        $res1 = ""
        $this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method Get -Headers $this.headers -SkipCertificateCheck -ResponseHeadersVariable "res1"
        #$res
        $this.ResultHeaders = $res1
    }    
    
    CreateRule([RuleDescription] $Rule,[SectionDescription] $ParentSection){#}, $SectionEtag){
        $SectionId = $ParentSection.id
        $StrEtag = ""+$ParentSection.Etag
        $head = $this.headers.Clone()       
        $head.Add("If-Match",$StrEtag)
        $uri = ""+$this.UriBase+"/api/4.0/firewall/globalroot-0/config/layer3sections/"+$SectionId+"/rules"
        $res1 = ""
        [xml] $xmlBody = $Rule.GetAsXML()
        if($Rule.ID -eq 0){ $Method = "Post" } else { $Method = "Put" 
        $uri += ("/"+$Rule.ID)
        }
        $this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method $Method -Headers $head -Body $xmlbody -ContentType "application/xml" -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
        $this.ResultHeaders = $res1   
    }
    DeleteRule([string] $RuleId,[SectionDescription] $ParentSection){
        $SectionId = $ParentSection.id
        $StrEtag = ""+$ParentSection.Etag
        $head = $this.headers.Clone()       
        $head.Add("If-Match",$StrEtag)
        $Method = "Delete"
        $res1 = ""
        $uri = ""+$this.UriBase+"/api/4.0/firewall/globalroot-0/config/layer3sections/"+$SectionId+"/rules/"+$RuleId
        #$this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method $Method -Headers $head -Body $xmlbody -ContentType "application/xml" -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
        try{
            $this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method $Method -Headers $head -ContentType "application/xml" -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
            $this.ResultHeaders = $res1   
        }catch{
            #If rule also deleted
            #Invoke-RestMethod : {"errorCode":100029,"details":"Invalid rule identifier : 1047.","rootCauseString":null,"moduleName":"vShield App","errorData":null}
        }
    }

    [SecurityGroup[]] QuerySecurityGroups(){
        #GET /api/2.0/services/securitygroup/scope/{scopeId}
        $head = $this.headers        
        $uri = ""+$this.UriBase+"/api/2.0/services/securitygroup/scope/globalroot-0"
        $res1 = ""

        $this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method Get -Headers $head -ContentType "application/xml" -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"

        $this.ResultHeaders = $res1   
        #Parse
        [SecurityGroup[]] $Groups = @()
        $this.Result | ForEach-Object {
            $_
            $SecGroup = New-Object SecurityGroup
            $SecGroup.ObjectID = $_.ObjectId
            $SecGroup.Name = $_.Name
            $SecGroup.Description = $_.description
            #$_.members
            #$_.dynamicMemberDefinition.dynamicSetDtoList
            $Groups += $SecGroup
        }
        return $Groups
    }

    [string[]] QueryVMBySecGroupID([string] $SecGroupId){
         #GET /api/2.0/services/securitygroup/{objectId}/translation/virtualmachines
         $head = $this.headers        
         $uri = ""+$this.UriBase+"/api/2.0/services/securitygroup/$SecGroupId/translation/virtualmachines"
         $res1 = ""       
         $this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method Get -Headers $head -ContentType "application/xml" -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
         $this.ResultHeaders = $res1   
         #Parse
         [string[]] $VMs = @()
         foreach($VMNode in $this.Result.vmNodes){
             $VMs += $VMNode.vmName
         }

         return $VMs
     }
    [SecurityGroup[]] QuerySecurityGroupsByVMId([string] $VMId){
        #GET /api/2.0/services/securitygroup/scope/{scopeId}
        $head = $this.headers        
        $uri = ""+$this.UriBase+"/api/2.0/services/securitygroup/lookup/virtualmachine/$VMId"
        $res1 = ""

        $this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method Get -Headers $head -ContentType "application/xml" -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
        $this.ResultHeaders = $res1   
        #Parse
        [SecurityGroup[]] $Groups = @()
        $this.Result.securityGroups | ForEach-Object {
            $_
            $SecGroup = New-Object SecurityGroup
            $SecGroup.Name = $_.Name
            $SecGroup.Description = $_.description
            #$_.members
            #$_.dynamicMemberDefinition.dynamicSetDtoList
            $Groups += $SecGroup
        }
        return $Groups
    }
    GetIPByHost([string] $VMId){
        #GET /api/2.0/services/securitygroup/scope/{scopeId}
        $head = $this.headers        
        $uri = ""+$this.UriBase+"/api/1.0/identity/hostIpMapping"
        $res1 = ""

        $this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method Get -Headers $head -ContentType "application/xml" -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
        $this.ResultHeaders = $res1   
    }

    [RuleDescription[]] QuerySectionRules([SectionDescription] $ParentSection){
        [RuleDescription[]] $Rules=$null
        $SectionId = $ParentSection.id
        #$StrEtag = ""+$ParentSection.Etag
        $uri = ""+$this.UriBase+"/api/4.0/firewall/globalroot-0/config/layer3sections/"+$SectionId

        $res1 = ""
        $this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method GET -Headers $this.headers -ContentType "application/xml" -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
        $this.ResultHeaders = $res1  
        $this.Result.rules | ForEach-Object {
            [Endpoint] $Source = New-Object Endpoint
            [Endpoint] $Destination = New-Object Endpoint            
            [RuleService] $Service = New-Object RuleService
            $_.services.serviceList | ForEach-Object {
                #name value type IsValid
                if($_.name -eq $null){
                    #This is RAW protocol
                    if($_.protocolName -eq "TCP") {
                        $ProtocolType = [RawProtocolType]::TCP
                    } elseif($_.protocolName -eq "UDP") {
                        $ProtocolType = [RawProtocolType]::UDP    
                    } elseif($_.protocolName -eq "ICMP") {
                        $ProtocolType = [RawProtocolType]::ICMP
                    } elseif($_.protocolName -eq "IGMP") {
                        $ProtocolType = [RawProtocolType]::IGMP
                    } elseif($_.protocolName -eq "AH") {
                        $ProtocolType = [RawProtocolType]::AH
                    } elseif($_.protocolName -eq "ESP") {
                        $ProtocolType = [RawProtocolType]::ESP
                    } elseif($_.protocolName -eq "GRE") {
                        $ProtocolType = [RawProtocolType]::GRE
                    } elseif($_.protocolName -eq "IPCOMP") {
                        $ProtocolType = [RawProtocolType]::IPCOMP
                    } elseif($_.protocolName -eq "L2TP") {
                        $ProtocolType = [RawProtocolType]::L2TP
                    } elseif($_.protocolName -eq "NBDG_BROADCAST") {
                        $ProtocolType = [RawProtocolType]::NBDG_BROADCAST
                    } elseif($_.protocolName -eq "NBNS_BROADCAST") {
                        $ProtocolType = [RawProtocolType]::NBNS_BROADCAST
                    } elseif($_.protocolName -eq "RSVP") {
                        $ProtocolType = [RawProtocolType]::RSVP
                    } elseif($_.protocolName -eq "SCTP") {
                        $ProtocolType = [RawProtocolType]::SCTP
                    }
                    $Service.RawProtocolEntrys += New-Object RawProtocolEntry($ProtocolType,$_.sourcePort,$_.destinationPort)
                }else{
                    #This is Service
                    $Service.ServiceEntrys += New-Object ServiceEntry($_.name,$_.value,$_.type)
                }
            }
            $_.sources.sourceList | ForEach-Object {
                if($_.type -eq "Ipv4Address"){
                    $Source.IpAddesses += New-Object Ipv4Addr($_.value)
                }elseif($_.type -eq "VirtualMachine"){
                    $Source.Objects += New-Object EndpointObject([EndpointObjectType]::VM,$_.name,$_.value)
                }elseif($_.type -eq "SecurityGroup"){
                    $Source.Objects += New-Object EndpointObject([EndpointObjectType]::SecurityGroup,$_.name,$_.value)
                } 
            }          
            $_.destinations.destinationList | ForEach-Object {
                if($_.type -eq "Ipv4Address"){
                    $Destination.IpAddesses += New-Object Ipv4Addr($_.value)
                }elseif($_.type -eq "VirtualMachine"){
                    $Destination.Objects += New-Object EndpointObject([EndpointObjectType]::VM,$_.name,$_.value)
                }elseif($_.type -eq "SecurityGroup"){
                    $Destination.Objects += New-Object EndpointObject([EndpointObjectType]::SecurityGroup,$_.name,$_.value)
                }
            }          
            $Rule = New-Object RuleDescription($_.name,$Source,$Destination,$Service)
            $Rule.ID = $_.ID
            $Rules += $Rule
        } 
        return $Rules
    }
    [RuleDescription[]] FilterRules([RuleDescription[]] $Rules,[Endpoint] $FilterSource,[Endpoint] $FilterDest, [RuleService] $FilterService){
        [RuleDescription[]] $Filtered = $null
        foreach($CurrentRule in $Rules) {
            #$CurrentRule = $_
            if($null -ne $CurrentRule){
                $Matched = $false
                $MatchedCount = 0

                #Поиск в источнике по IP адресам фильтра
                foreach($FilterIP in $FilterSource.IpAddesses) {
                    #$FilterIP = $_
                    if($CurrentRule.Source.ContainIp($FilterIp)){
                        $Matched = $true
                    }
                    if($Matched -eq $true) {
                        $MatchedCount+=1
                        Break
                    }
                }
                #Поиск в источнике по объектам фильтра (группы безопасности, порт группы etc)
                #В фильтре скорее всего только имя машины, мак адрес, или чет такое, в правиле все что угодно
                $Matched = $false
                if(-not $Matched){
                    foreach($FilterObject in $FilterSource.Objects){
                          $Matched = $CurrentRule.Source.MatchFilterCriteria($this, $FilterObject)
                          if($Matched -eq $true) {
                            $MatchedCount+=1
                            Break
                            }
                    }
                }

                #Не нашли по источнику, поищем по назначению
                $Matched = $false
                if($Matched -ne $true){
                    foreach($FilterIP in $FilterDest.IpAddesses) {
                        #$FilterIP = $_
                        if($CurrentRule.Destination.ContainIp($FilterIp)){
                            $Matched = $true
                        }
                        if($Matched -eq $true) {
                            $MatchedCount+=1
                            Break
                        }
                    }
                }
                #Не нашли ранее, поищем по портам сервисам?
                $Matched = $false
                if($Matched -ne $true){
                    #Find by raw protocols
                    $Matched = $CurrentRule.Service.MatchFilterCriteria($FilterService)
                    if($Matched -eq $true) {
                        $MatchedCount+=1
                    }                    
                }

                #if($Matched -eq $true) { $Filtered += $CurrentRule}
                if($MatchedCount -ge 2) { $Filtered += $CurrentRule}
            }
        }
        return [RuleDescription[]] $Filtered
    }

    [SectionDescription] GetRuleSection(){
        $this.CallRestAPI("/api/4.0/firewall/globalroot-0/config")
        $SectionGroupID = $this.Result.layer3Sections.layer3Sections[0].id
        $this.CallRestAPI("/api/4.0/firewall/globalroot-0/config/layer3sections/$SectionGroupID")
        [SectionDescription] $ParentSection = New-Object SectionDescription
        $ParentSection.id =  $SectionGroupID
        $Etag = $this.ResultHeaders["Etag"].Replace('"','')
        $ParentSection.Etag = 0+($Etag)
        return [SectionDescription] $ParentSection
    }

    CreateSecurityGroup([SecurityGroup] $SecurityGroup){
        $uri = ""+$this.UriBase+"/api/2.0/services/securitygroup/bulk/globalroot-0"
        #$uri = ""+$this.UriBase+"/api/2.0/services/securitygroup/bulk/globalroot-0"
        $head = $this.headers
        try{ 
            $head.Add("Content-Type","application/xml")       
        }catch{}
        $res1 = ""
        #$Method = "POST"
        [xml] $xmlBody = $SecurityGroup.GetAsXML()
        $xmlBody.InnerXml
        if($SecurityGroup.ObjectID -eq ""){ $Method = "Post" } else { 
            $Method = "Put" 
            $uri += ("/"+$SecurityGroup.ObjectID)
        }
        $this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method $Method -Headers $head -Body $xmlBody -ContentType "application/xml" -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
        #$this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method $Method -Headers $head -Body $xmlBody -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
        $this.ResultHeaders = $res1   

    }

    PutVMToSecurityGroup([String] $SGName,[string] $VMName){
        $uri = ""+$this.UriBase+"/api/2.0/services/securitygroup/$SGName/members/$VMName"
        #$uri = ""+$this.UriBase+"/api/2.0/services/securitygroup/bulk/globalroot-0"
        $head = $this.headers 
        #$head.Add("Content-Type","application/xml")       
        $res1 = ""
        $Method = "Put" 

        #$this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method $Method -Headers $head -Body $xmlBody -ContentType "application/xml" -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
        $this.Result = Invoke-RestMethod -Uri $uri -Credential $this.cred -Method $Method -Headers $head  -SkipHeaderValidation -SkipCertificateCheck -ResponseHeadersVariable "res1"
        $this.ResultHeaders = $res1   

    }    

}

function CreateRule([RestController] $obj){
    <#
    #$Obj.QuerySecurityGroups()
    [SecurityGroup] $SG = New-Object SecurityGroup
    $SG.Name = "AutoSource"
    $SG.Description = "Test API ICL"
    $SGSME = New-Object SecurityGroupStaticMemberEntry
    $SGSME.Type = [SecurityGroupMemberTypeType]::VM
    $SGSME.Value = "vm-198"
    $SG.Include += $SGSME
    $Obj.CreateSecurityGroup($SG)
    $SGName = $Obj.Result
    #$Groups = $Obj.QuerySecurityGroups()
    #$Obj.CallRestAPI("/api/3.0/ai/securitygroup/$SGName")
    #$Obj.CallRestAPI("/api/2.0/services/securitygroup/$SGName")
    #$Obj.Result
    $Obj.PutVMToSecurityGroup($SGName,"vm-198")

    Break
    #>
    [SectionDescription] $ParentSection = $obj.GetRuleSection()
    #Make/Update Rule
    [Endpoint] $Source = New-Object Endpoint
    [Endpoint] $Destination = New-Object Endpoint
    [Ipv4Addr[]] $SourceIpAddesses=@()
    [Ipv4Addr[]] $DestIpAddesses=@()
    $QueryJson[1] | ForEach-Object {
        $SourceIpAddesses +=New-Object Ipv4Addr($_[1])
    }
    $Source.IpAddesses =$SourceIpAddesses
    <#Create Source Security Group
    [SecurityGroup] $SG = New-Object SecurityGroup
    $SG.Name = "AutoSource"
    $SG.Description = "Test API ICL"
    $SGSME = New-Object SecurityGroupStaticMemberEntry
    $SGSME.Type = [SecurityGroupMemberTypeType]::VM
    $SGSME.Value = "vm-198"
    $SG.Include += $SGSME
    $SGName = $Obj.CreateSecurityGroup($SG)
    #$SG.ObjectID = $Obj.Result
    #>
    if($RegExp -ne ""){
        $SGName = CreateSecurityGroupDynMembers -obj $obj -Network "network-244" -RegExp $RegExp
        $Source.Objects += New-Object EndpointObject([EndpointObjectType]::SecurityGroup,$SGName,$SGName)        
    }

    $QueryJson[2] | ForEach-Object {
        $DestIpAddesses +=New-Object Ipv4Addr($_[0])
    }
    $Destination.IpAddesses = $DestIpAddesses

    [RuleService] $Service = New-Object RuleService
    #$Proto = $QueryJson[3][0]
    <#
    $QueryJson[3]| ForEach-Object {
        $Proto = $_[0]
        $ProtoPort = $_[1]
        if($Proto -eq "TCP"){
            $ProtoType = [RawProtocolType]::TCP
        }else{
            $ProtoType = [RawProtocolType]::UDP
        }
        $Service.RawProtocolEntrys += New-Object RawProtocolEntry($ProtoType,$ProtoPort,$ProtoPort)
    }
    #>
    $QueryJson[3]| ForEach-Object {
        $Proto = $_[0]
        $ProtoPorts = $_[1].ToString().Split(",")
        if($Proto -eq "TCP"){
            $ProtoType = [RawProtocolType]::TCP
        }else{
            $ProtoType = [RawProtocolType]::UDP
        }
        $ProtoPorts | ForEach-Object {
            $ProtoPort = $_.ToString().TrimStart(" ").TrimEnd(" ")
            $RawProtocolEntry = New-Object RawProtocolEntry($ProtoType,$ProtoPort,$ProtoPort)
            if(-not $Service.MatchFilterCriteria([RawProtocolEntry] $RawProtocolEntry)){
                $Service.RawProtocolEntrys += $RawProtocolEntry
            }
        }
    }

    #$Service.ServiceEntrys += New-Object ServiceEntry("Active Directory Server","application-213","Application")
    [RuleDescription] $Rule = New-Object RuleDescription("test1920",$Source,$Destination,$Service)
    #$obj.CallRestAPI("/api/4.0/firewall/globalroot-0/config")
    #$Rule.ID = 1020
    $obj.CreateRule($Rule,$ParentSection)    
}
function ModifyRule([RestController] $obj,[string] $RuleId){
        #Modify rule Add
        $Rules = $obj.QuerySectionRules($obj.GetRuleSection())
        $Rules | Where-Object -Property ID -EQ  $RuleId | ForEach-Object  {
            $Rule = $_
            #Add new parameters to Rule
            $QueryJson[1] | ForEach-Object {
                $Ipv4Address = New-Object Ipv4Addr($_[1])
                if(-not $Rule.Source.ContainIp($Ipv4Address)){
                    $Rule.Source.IpAddesses += $Ipv4Address
                }
            }            
            $QueryJson[2] | ForEach-Object {
                $Ipv4Address = New-Object Ipv4Addr($_[0])
                if(-not $Rule.Destination.ContainIp($Ipv4Address)){
                    $Rule.Destination.IpAddesses += $Ipv4Address
                }
            } 
            #pwsh.exe C:\Users\RabadanovD\Desktop\testRestGPB.ps1 -NsxManagerIp 10.1.101.72 -UpdateRule -RuleId 1019 -JsonQueryAsString "['Другие администраторы', [['espi0108', '10.5.242.9'], ['espi0109', '10.5.242.3'], ['espi0110', '10.5.242.4']], [['10.96.96.96', 'none'], ['10.96.96.97', 'none'], ['10.96.96.98', 'none']],  [['TCP','22,80,8080'], ['UDP', '53, 514']], 'Тестовый комментарий второй']"      
            $QueryJson[3]| ForEach-Object {
                $Proto = $_[0]
                $ProtoPorts = $_[1].ToString().Split(",")
                if($Proto -eq "TCP"){
                    $ProtoType = [RawProtocolType]::TCP
                }else{
                    $ProtoType = [RawProtocolType]::UDP
                }
                $ProtoPorts | ForEach-Object {
                    $ProtoPort = $_.ToString().TrimStart(" ").TrimEnd(" ")
                    $RawProtocolEntry = New-Object RawProtocolEntry($ProtoType,$ProtoPort,$ProtoPort)
                    if(-not $Rule.Service.MatchFilterCriteria([RawProtocolEntry] $RawProtocolEntry)){
                        $Rule.Service.RawProtocolEntrys += $RawProtocolEntry
                    }
                }
            }
                #MatchFilterCriteria([RawProtocolEntry] $FilterRawProtocolEntry)
                #Write back rule
                $obj.CreateRule($Rule,$obj.GetRuleSection())         
        }
}
function DeleteRule([RestController] $obj, [string] $RuleId){
    $obj.DeleteRule($RuleId,$obj.GetRuleSection())
}
function ShowRules([RestController]$obj){
    $RulesText = ""
    $Rules = $obj.QuerySectionRules($obj.GetRuleSection())
    $Rules | ForEach-Object {
       $Rule = $_ 
        $RuleText = "Id/Name: $($Rule.ID)/$($Rule.Name)`r`n"
        $SRCS = "SRC: "
        $DST = "DST: "
        $SRVCS = "SRV: "
        if($null -ne $Rule.Source.IpAddesses){
            $Rule.Source.IpAddesses | ForEach-Object {
                $SRCS += $_.GetAsString()+","
            }
        }
        if($SRCS.Trim(" ") -eq "SRC:") { $SRCS = "SRC: Any"}
        $RuleText += "$SRCS`r`n"
        if($null -ne $Rule.Destination.IpAddesses){
            $Rule.Destination.IpAddesses | ForEach-Object {
                $DST += $_.GetAsString()+","
            }
        }
        if($DST.Trim() -eq "DST:") { $DST = "DST: Any"}
        $RuleText += "$DST`r`n"
        if($null -ne $Rule.Service.RawProtocolEntrys){
            $Rule.Service.RawProtocolEntrys | ForEach-Object {
                $SRVCS += $_.GetAsString()+","
            }
        }
        if($null -ne $Rule.Service.ServiceEntrys){
            $Rule.Service.ServiceEntrys | ForEach-Object {
                $SRVCS += $_.Name+","#$_.GetAsString()+","
                #$SRVCS += "("+$_.Name+","+$_.value+")"#$_.GetAsString()+","
            }
        }
        $SRVCS = $SRVCS.TrimEnd(",")
        if($SRVCS.Trim() -eq "SRV:") { $SRVCS = "SRV: Any"}
        $RuleText += "$SRVCS`r`n"
        $RulesText += "$RuleText`r`n"
    }
    $RulesText
}

function GetIPByHost([RestController] $obj,[string] $HostName){
    $obj.GetIPByHost($HostName)
    $obj.Result
}

function CreateSecurityGroupDynMembers([RestController]$obj,[string] $network="",[string] $RegExp=""){
    [SecurityGroup] $SG = New-Object SecurityGroup
    $SG.Name = "AutoSG$(Get-Date)"
    $SG.Description = "Test API ICL"
    if($network -eq ""){
        $SG.DynamicMemberEntrys += New-Object SecurityGroupDynamicMemberEntry([SecurityGroupDynamicMemberFilterField]::Entity,[SecurityGroupDynamicMemberFilterType]::BelongsTo,"network-244")
        $SG.DynamicMemberEntrys += New-Object SecurityGroupDynamicMemberEntry([SecurityGroupDynamicMemberFilterField]::VMName,[SecurityGroupDynamicMemberFilterType]::Contains,"test")
    }else{
        $SG.DynamicMemberEntrys += New-Object SecurityGroupDynamicMemberEntry([SecurityGroupDynamicMemberFilterField]::Entity,[SecurityGroupDynamicMemberFilterType]::BelongsTo,"$network")
        $SG.DynamicMemberEntrys += New-Object SecurityGroupDynamicMemberEntry([SecurityGroupDynamicMemberFilterField]::VMName,[SecurityGroupDynamicMemberFilterType]::MatchRegex,"$RegExp")
    }
    $Obj.CreateSecurityGroup($SG)
    $SG.ObjectID = $Obj.Result
    #$Obj.CreateSecurityGroup($SG)
    $SG.ObjectID
}
function Main(){
    "********************************************************************"+(Get-Date)
    $obj = New-Object RestController($NsxManagerUser,$NsxManagerPassword,"https://$NsxManagerIP")
    if(-not $InteractiveMode){
        Write-Host "Automatic mode $Action"
        if([ModuleAction] $Action -eq [ModuleAction]::Delete) {
            Write-Host "Action: Delete rule"
            DeleteRule -obj $obj -RuleId $RuleId
        }elseif([ModuleAction] $Action -eq [ModuleAction]::Update) {
            Write-Host "Action: Update rule"
            ModifyRule -obj $obj -RuleId $RuleId
        }elseif(([ModuleAction] $Action) -eq [ModuleAction]::Show) {
            Write-Host "Action: Show rule"
            ShowRules($obj)
        }elseif([ModuleAction] $Action -eq [ModuleAction]::Create) {
            Write-Host "Action: Create rule"
            CreateRule($obj)
        }else{
            Write-Host "Wrong parameter action"
        }
    }else{
        Write-Host "Interactive mode"
        $finish = $false
        $Rule = $null
        do{
            #Prepare section
            Write-Host "1. Exit"
            Write-Host "2. Create firewall rule with dynamic Secutiy Group (PortGroup=testvlan2 VM=NSX-test2); Dest:IP=192.168.2.8 ;Service: TCP=40 to 566"
            Write-Host "3. Get filtered firewall rules by VM+Port (VM=NSX-test2; Service: TCP=40 to 566)"
            Write-Host "4. Get filtered firewall rules by VM+Dest IP (VM=NSX-test2; Dest:IP=192.168.2.8)"
            Write-Host "5. Get filtered firewall rules by VM only (empty results)"
            Write-Host "6. Get filtered firewall rules by ServiceName (Active Directory + DHCP)"
            Write-Host "7. Query Security groups"
            Write-Host "8. Create Security group (VMName, PortGroupName)"
            $ans = Read-Host "Enter your choice number"
            if($ans -eq "1"){
                $finish = $true
            }elseif($ans -eq "2"){
                #Prepare section
                [SectionDescription] $ParentSection = $obj.GetRuleSection()
                #Make/Update Rule
                [Endpoint] $Source = New-Object Endpoint
                [Endpoint] $Destination = New-Object Endpoint
                $SGName = CreateSecurityGroupDynMembers -obj $obj
                $Source.Objects += New-Object EndpointObject([EndpointObjectType]::SecurityGroup,$SGName,$SGName)
                #$Source.IpAddesses +=New-Object Ipv4Addr(192,168,6,1)
                #$Source.IpAddesses +=New-Object Ipv4Addr(192,168,6,2)
                $Destination.IpAddesses +=New-Object Ipv4Addr(192,168,2,8)
                $Destination.IpAddesses +=New-Object Ipv4Addr(192,168,4,16)
                [RuleService] $Service = New-Object RuleService
                $Service.RawProtocolEntrys += New-Object RawProtocolEntry([RawProtocolType]::TCP,40,556)
                $Service.ServiceEntrys += New-Object ServiceEntry("Active Directory Server","application-213","Application")
                $Service.ServiceEntrys += New-Object ServiceEntry("DHCP-Client","application-482","Application")
                $Service.ServiceEntrys += New-Object ServiceEntry("DHCP-Server","application-162","Application")
                [RuleDescription] $Rule = New-Object RuleDescription("test$(Get-Date)",$Source,$Destination,$Service)
                #$obj.CallRestAPI("/api/4.0/firewall/globalroot-0/config")
                #$Rule.ID = 1020
                $obj.CreateRule($Rule,$ParentSection)        
            }elseif($ans -eq "3"){
                [SectionDescription] $ParentSection = $obj.GetRuleSection()
                $Rules = $obj.QuerySectionRules($ParentSection)
                [Endpoint] $FilterSource = New-Object Endpoint
                #$FilterSource.Objects += New-Object EndpointObject([EndpointObjectType]::VM,"esxi-nsx-test-02.iss.icl.kazan.ru","vm-198")
                $FilterSource.Objects += New-Object EndpointObject([EndpointObjectType]::VM,"nsx-test2","vm-245")
                
                [Endpoint] $FilterDest = New-Object Endpoint
                [RuleService] $FilterService = New-Object RuleService
                $FilterService.RawProtocolEntrys += New-Object RawProtocolEntry([RawProtocolType]::TCP,40,556)
                
                $Filtered = $obj.FilterRules($Rules,$FilterSource,$FilterDest,$FilterService)
                $Filtered
            }elseif($ans -eq "4"){
                [SectionDescription] $ParentSection = $obj.GetRuleSection()
                $Rules = $obj.QuerySectionRules($ParentSection)
                [Endpoint] $FilterSource = New-Object Endpoint
                $FilterSource.Objects += New-Object EndpointObject([EndpointObjectType]::VM,"nsx-test2","vm-245")
                #$FilterSource.IpAddesses +=New-Object Ipv4Addr(192,168,1,1)
                #$FilterSource.IpAddesses +=New-Object Ipv4Addr(10,112,1,1)
                
                [Endpoint] $FilterDest = New-Object Endpoint
                $FilterDest.IpAddesses += New-Object Ipv4Addr(192,168,2,8)
                [RuleService] $FilterService = New-Object RuleService
                
                $Filtered = $obj.FilterRules($Rules,$FilterSource,$FilterDest,$FilterService)
                $Filtered
            }elseif($ans -eq "5"){
                [SectionDescription] $ParentSection = $obj.GetRuleSection()
                $Rules = $obj.QuerySectionRules($ParentSection)
                [Endpoint] $FilterSource = New-Object Endpoint
                $FilterSource.Objects += New-Object EndpointObject([EndpointObjectType]::VM,"nsx-test2","vm-245")
                
                [Endpoint] $FilterDest = New-Object Endpoint
                [RuleService] $FilterService = New-Object RuleService
                #$FilterService.RawProtocolEntrys += New-Object RawProtocolEntry([RawProtocolType]::TCP,0,8080)
                
                $Filtered = $obj.FilterRules($Rules,$FilterSource,$FilterDest,$FilterService)
                $Filtered
            }elseif($ans -eq "6"){
                [SectionDescription] $ParentSection = $obj.GetRuleSection()
                $Rules = $obj.QuerySectionRules($ParentSection)
                [Endpoint] $FilterSource = New-Object Endpoint
                
                [Endpoint] $FilterDest = New-Object Endpoint
                [RuleService] $FilterService = New-Object RuleService
                $FilterService.ServiceEntrys += New-Object ServiceEntry("Active Directory Server","value","type")
                $FilterService.ServiceEntrys += New-Object ServiceEntry("DHCP-Client","value","type")
                
                $Filtered = $obj.FilterRules($Rules,$FilterSource,$FilterDest,$FilterService)
                $Filtered
            }elseif($ans -eq "7"){
                $Result = $Obj.QuerySecurityGroups()
                $Result
            }elseif($ans -eq "8"){
                CreateSecurityGroupDynMembers -obj $obj
            }elseif($ans -eq "9"){
                GetIPByHost -obj $obj -HostName "nsx-test2"
            }
        
        }while($finish -eq $false)
    }
    Write-Host "Completed"
}

Main