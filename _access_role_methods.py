######################################################
#
#
# Module to represent working methods with access roles
#               throught API
#
#
# Python 3.6
#####################################################

# A package for reading passwords without displaying them on the console.

from pprint import pprint
import sys, os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# cpapi is a library that handles the communication with the Check Point management server.
from cpapi import APIClient, APIClientArgs

#This method for one-time adding object (without hold of established connection with SMS)
#That means that we establish connection with SMS through zero point - without tracking state of connection
def add_access_role(sms,username,password,name,**params):
    '''
    Add access role
    :param sms: IP address of SMS
    :param username: API username
    :param password: API user password
    :param name: name of access role
    :param params: [optional] Dict of optional params, that defined in API
    :return: JSON representaion of API-response (dictionary)
    '''
    # Define parameters of establishing connection with SMS
    # on PoC only IP of SMS can define - hardcoded, after can refactor to pass another params that define in class
    client_args = APIClientArgs(server=sms)

    with APIClient(client_args) as client:
        # The API client, would look for the server's certificate SHA1 fingerprint in a file.
        # If the fingerprint is not found on the file, it will ask the user if he accepts the server's fingerprint.
        # In case the user does not accept the fingerprint, exit the program.
        if client.check_fingerprint() is False:
            print("Could not get the server's fingerprint - Check connectivity with the server.")
            exit(1)
        # login to server:
        login_res = client.login(username, password)

        # Verify (is all right?)
        if login_res.success is False:
            print("Login failed:\n{}".format(login_res.error_message))
            exit(1)

        result = _add_access_role(client,name,**params)

        if result:
            # publish the result
            publish_res = client.api_call("publish", {})
            if publish_res.success:
                print("The changes were published successfully.")
            else:
                print("Failed to publish the changes.")
        else:
            print('Access role already exist')
            # exit(-1)
        # Logout from SMS automaticaly when destroyed APIClient object

    return result

def change_access_role(sms, username, password, name, **params):
    '''
    Change access role
    :param sms: IP address of SMS
    :param username: API username
    :param password: API user password
    :param name: name of access role
    :param params: [optional] Dict of optional params, that defined in API
    :return: JSON representaion of API-response (dictionary)
    '''
    # Define parameters of establishing connection with SMS
    # on PoC only IP of SMS can define - hardcoded, after can refactor to pass another params that define in class
    client_args = APIClientArgs(server=sms)

    with APIClient(client_args) as client:
        # The API client, would look for the server's certificate SHA1 fingerprint in a file.
        # If the fingerprint is not found on the file, it will ask the user if he accepts the server's fingerprint.
        # In case the user does not accept the fingerprint, exit the program.
        if client.check_fingerprint() is False:
            print("Could not get the server's fingerprint - Check connectivity with the server.")
            exit(1)
        # login to server:
        login_res = client.login(username, password)

        # Verify (is all right?)
        if login_res.success is False:
            print("Login failed:\n{}".format(login_res.error_message))
            exit(1)

        result = _change_access_role(client,name,**params)

        if result:
            # publish the result
            publish_res = client.api_call("publish", {})
            if publish_res.success:
                print("The changes were published successfully.")
            else:
                print("Failed to publish the changes.")
        else:
            exit(-1)
        # Logout from SMS automaticaly when destroyed APIClient object

    return result

#This method applied to add access role while we use established connection with SMS.
#There needless to login and publish changes - cause we bring them to higher layer of abstraction.
#But you should put as argument APIClient object where stored initial params of session with SMS.
def _add_access_role(client,name,**params):
    '''
    Add access role through API, while connection have been established
    :param client: [APIClient] APIClient object that stored connection with SMS
    :param name: [str] Name of role
    :param params: [dict] [optional] Parameters that can be add withing API-request (see API reference)
    :return: If success - JSON-representation of API-response  or None - if fail
    '''
    payload = dict()
    #Work with parametrs that have been transferred throught **params
    #Will do one common dictionary - payload
    if params:
        payload['name'] = name
        payload.update(params)
    else:
        payload['name'] = name

    # Add access role
    add_access_role_response = client.api_call("add-access-role", payload)

    if add_access_role_response.success:
        print(f"The access role: '{payload['name']}' has been added successfully")
    else:
        print(f"Failed to add access role: '{payload['name']}', Error:\n{add_access_role_response.error_message}")
        return None
    return add_access_role_response.data

def _change_access_role(client,name,**params):
    '''
    Change access role object with API
    :param client: [APIClient] APIClient object that stored connection with SMS
    :param name: [str] Name of role
    :param params: [dict] [optional] Parameters that can be add withing API-request (see API reference)
    :return: If success - JSON-representation of API-response  or None - if fail
    '''
    payload = dict()
    #Work with parametrs that have been transferred throught **params
    #Will do one common dictionary - payload
    if params:
        payload['name'] = name
        payload.update(params)
    else:
        payload['name'] = name

    # Change access role
    change_access_role_response = client.api_call("set-access-role", payload)

    if change_access_role_response.success:
        print(f"The access role: '{payload['name']}' has been changed successfully")
    else:
        print(f"Failed to change access role: '{payload['name']}', Error:\n{change_access_role_response.error_message}")
        return None
    return change_access_role_response.data


if __name__=='__main__':

    api_server = '10.1.101.53'
    username = 'api_user'
    password = 'vpn123'
    name = input('Input name of newly created access role: ')
    params = {'networks':'10.1.1.1/32',
              'users':{'source':'',
                       'selection':'ISSVPNUsers'}}
    pprint(add_access_role(api_server, username, password,name,**params))
